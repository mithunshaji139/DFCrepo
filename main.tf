 provider "aviatrix" {
   controller_ip           = "3.209.232.244"
   username                = "admin"
   password                = "Dhruvts@123"
   skip_version_validation = true
 }
 
terraform {
  required_providers {
    aviatrix = {
    source = "AviatrixSystems/aviatrix"
    version = "3.1.4"
    }
  }
}
 
# Create an Aviatrix Distributed Firewalling Policy List
 
resource "aviatrix_distributed_firewalling_policy_list" "app1-app2" {
  policies {
    name             = "app1-app2"
    action           = "PERMIT"
    priority         = 2
    protocol         = "ANY"
    logging          = false
    watch            = false
    src_smart_groups = [
      "86a13a22-6c8d-4e80-8cef-7d7aabc06b12"
    ]
    dst_smart_groups = [
      "744150fd-2dd3-4324-82f5-77298231a55d"
    ]
  }

  policies {
    name             = "app2-app1"
    action           = "DENY"
    priority         = 1
    protocol         = "ANY"
    logging          = false
    watch            = false
    src_smart_groups = [
      "744150fd-2dd3-4324-82f5-77298231a55d"
    ]
    dst_smart_groups = [
      "86a13a22-6c8d-4e80-8cef-7d7aabc06b12"
    ]
  }
 
}
